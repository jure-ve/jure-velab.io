---
title: '¿Cómo se instalan las extensiones Firebird/InterBase en PHP 7.4/8?'
date: 2021-07-06
publishdate: 2017-07-06
draft: false
category: ['php']
---

Te recomiendo visitar la página del controlador de Firebird para *PHP* https://github.com/FirebirdSQL/php-firebird donde encontraras los pasos para compilar los drivers, te coloco un pequeño resumen con los pasos para hacerlo en Linux Mint 20 / Ubuntu:

### Construye el controlador en Linux

En primer lugar, tienes que cumplir algunos requisitos. Esto significa que necesitamos instalar el comando *phpize*. El comando *phpize* se usa para preparar el entorno de compilación para una extensión *PHP*. Instala el comando *phpize*. Esto generalmente se hace instalando el paquete *php7-devel* o *php8-devel* usando el administrador de paquetes del sistema. También instale la biblioteca *fbclient* y los paquetes de desarrollador.

El comando en Linux Mint 20 / Ubuntu es:

    sudo apt-get install php-dev firebird-dev firebird3.0 firebird3.0-common firebird3.0-server

Ahora asegúrate de proporcionar *fbclient.so* y los archivos de encabezado (*ibase.h*). Estos son necesarios para compilar. Puede especificar la ruta de inclusión para el archivo *ibase.h* con CPPFLAGS como puede ver en la siguiente lista.

    $ git clone https://github.com/FirebirdSQL/php-firebird.git
    $ cd php-firebird
    $ phpize7.4
    $ CPPFLAGS=-I/usr/include/firebird ./configure --with-php-config=/usr/bin/php-config7.4
    $ make

Si el proceso de configuración finaliza sin problemas, recibirás el siguiente mensaje:

    $ Build complete.
    $ Don't forget to run 'make test'.

Puedes encontrar el archivo *interbase.so* en el directorio *php-firebird/modules*. 

### Configurar la extensión

1. Busque el directorio *PHP* de las extensiones:

`php-config --extension-dir`

2. Mueva el archivo *interbase.so* de la extensión al directorio indicado en el paso anterior.

3. Busque tu archivo *php.ini*.

`php -ini | grep 'Loaded Configuration File'`

4. Agregue la siguiente línea a tu *php.ini*:

`extensión = interbase.so`

5. Reinicia tu servidor web.  
    
6. Verifique la extensión:

`php -m`

### Limpia tu directorio de trabajo  

Una vez que haya creado los datos binarios, se crearán muchos archivos temporales en su directorio de trabajo. Estos se pueden eliminar con el comando *phpize --clean*. Entonces tienes un directorio limpio nuevamente.

Y ya tienes tienes tu *PHP* listo para conectarte con tu servidor *Firebird*. 

